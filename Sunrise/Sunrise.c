/*
 * Sunrise.c
 *
 * Created: 10/13/2013 9:09:39 PM
 *  Author: Jakub
 */ 

/* ATtiny13 pinout
                                     -------
  (PCINT5 / ~RESET / ADC0 / dW) PB5 |       | VCC
         (PCINT3 / CLKI / ADC3) PB3 |  t13  | PB2 (SCK / ADC1 / T0 / PCINT2)
                (PCINT4 / ADC2) PB4 |       | PB1 (MISO / AIN1 / OC0B / INT0 / PCINT1)
				                GND |       | PB0 (MOSI / AIN0 / OC0A / PCINT0)
								     -------

	ISP pinout
								  ----
							MISO |    | VCC
							 SCK |    | MOSI
							 RST |    | GND
							      ----
*/

#define F_CPU 9600000UL  // 9.6 MHz
#include <avr/io.h>
//#include <avr/cpufunc.h>
#include <avr/interrupt.h>
#include <avr/sleep.h>

// high on this pin triggers sunrise
static const uint8_t ALARM_PIN = PB2;

// increase/decrease light - low-triggered
static const uint8_t PLUS_PIN = PB3;
static const uint8_t MINUS_PIN = PB4;

// LED strip PWM output from OC0A
static const uint8_t STRIP = PB0;

// status LEDs
static const uint8_t LED = PB1;

static const uint16_t SLOW = F_CPU / 256 / 8 * (30*60/256); // to go from 0 to 256 in 30 minutes
static const uint16_t DEBOUNCE = F_CPU / 256 / 8 / 10 /256; // debounce = 100ms

static volatile enum states {IDLE, START_HOLDOFF, HOLDOFF, START_RISING, RISING, SHINING, SETTING} state = IDLE;

static inline void alarm_interrupt_enable() { PCMSK |= _BV(ALARM_PIN); }
static inline void alarm_interrupt_disable() { PCMSK &= ~_BV(ALARM_PIN); }
static inline void plus_interrupt_enable() { PCMSK |= _BV(PLUS_PIN); }
static inline void plus_interrupt_disable() { PCMSK &= ~_BV(PLUS_PIN); }
static inline void minus_interrupt_enable() { PCMSK |= _BV(MINUS_PIN); }
static inline void minus_interrupt_disable() { PCMSK &= ~_BV(MINUS_PIN); }

int main(void) {
	
	// set output pins
	DDRB = _BV(STRIP) || _BV(LED);

	// select clock source for PWM and prescaler
	TCCR0B = _BV(CS01); // clk_IO / 8
	
	// select fast PWM mode
	// select compare output mode: clear on match, set on TOP
	TCCR0A = _BV(WGM01) | _BV(WGM00) | _BV(COM0A1);

	// enable timer interrupt
	TIMSK0 = _BV(OCIE0A);
	
	// enable alarm and button-press interrupt
	alarm_interrupt_enable();
	plus_interrupt_enable();
	minus_interrupt_enable();
	GIMSK = _BV(PCIE);
	
	sei();	

    while(1) {
		sleep_mode();
	}
}

ISR(TIM0_COMPA_vect) {
	static uint16_t timer;
	static uint16_t holdoff;
	static uint8_t shining_counter;
	switch(state) {
		
		case START_HOLDOFF:
			holdoff = DEBOUNCE;
			
		case HOLDOFF:
			if(--holdoff == 0) {
				PORTB &= ~_BV(LED); // clear button press signalization
				state = IDLE;
			}
			break;
		
		case START_RISING:
			timer = SLOW;
			state = RISING;

		case RISING:
			if(--timer == 0) {
				if(OCR0A < 0xFF) {
					++OCR0A;
					timer = SLOW;
				} else {
					PORTB &= ~_BV(LED); // clear led
					alarm_interrupt_enable();
					timer = SLOW;
					shining_counter = 0xFF;
					state = SHINING;
				}
			}
			break;

		case SHINING:
			if(--timer == 0) {
				if(shining_counter > 0) {
					--shining_counter;
					timer = SLOW;
				} else {
					timer = SLOW;
					state = SETTING;
				}
			}
			break;

		case SETTING:
			if(--timer == 0) {
				if(OCR0A > 0) {
					--OCR0A;
					timer = SLOW;
				} else {
					PORTB &= ~_BV(LED); // clear led
					alarm_interrupt_enable();
					state = IDLE;
				}
			}
			break;
	}
}

ISR(PCINT0_vect) {
	// a button was pressed (transition from HIGH to LOW) or alarm triggered (transition from LOW to HIGH)

	// alarm
	if(PINB & _BV(ALARM_PIN)) {
		alarm_interrupt_disable();
		plus_interrupt_enable();
		minus_interrupt_enable();
		state = START_RISING;
	} else 
	
	// plus button
	if(!(PINB & _BV(PLUS_PIN))) {
		PORTB |= _BV(LED); // signal button press
		if(OCR0A < 0xFF) {
			++OCR0A;
			plus_interrupt_disable();
			alarm_interrupt_enable();
			state = START_HOLDOFF;
		}
	} else
	
	// minus button
	if(!(PINB & _BV(MINUS_PIN))) {
		PORTB |= _BV(LED); // signal button press
		if(OCR0A > 0) {
			--OCR0A;
			minus_interrupt_disable();
			alarm_interrupt_enable();
			state = START_HOLDOFF;
		}
	}
}